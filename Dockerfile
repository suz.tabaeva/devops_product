FROM ubuntu

WORKDIR /root
ADD . /root

RUN apt update
RUN apt install maven -y
RUN mvn clean test
RUN mvn clean package
RUN mvn clean test
RUN mvn clean package

CMD mvn tomcat7:run